/* Теоретичні питання 
1.У JavaScript існує декілька способів для створення та додавання нових DOM-елементів:

document.createElement(tagName): створює новий елемент з заданим ім'ям тегу.
element.appendChild(newChild): додає новий елемент в кінець дочірніх елементів поточного елементу.
element.insertBefore(newNode, referenceNode): вставляє новий елемент перед вказаним дочірнім елементом.
element.insertAdjacentElement(position, newElement): вставляє новий елемент відносно поточного елементу. Позиції можуть бути "beforebegin", "afterbegin", "beforeend", "afterend".
element.innerHTML: дозволяє додавати HTML-код в середину елементу (не рекомендується для створення великих або складних елементів з міркувань безпеки та продуктивності).

2.Знайти елемент з класом "navigation" на сторінці за допомогою методу document.querySelector('.navigation').
Використати метод element.remove() для видалення знайденого елементу.

3.Для вставки DOM-елементів перед/після іншого DOM-елемента можна використовувати такі методи:

element.insertBefore(newElement, referenceElement): вставляє новий елемент перед вказаним дочірнім елементом.
element.insertAdjacentElement(position, newElement): вставляє новий елемент відносно поточного елементу. Можливі позиції:
"beforebegin" - перед самим елементом.
"afterbegin" - на початку всередині елемента.
"beforeend" - в кінці всередині елемента.
"afterend" - після самого елемента.
*/

// Практичні завдання
// 1.

// const newLink = document.createElement('a');
// newLink.textContent = 'Learn More';
// newLink.href = '#';

// const footer = document.querySelector('footer');
// const lastParagraph = footer.querySelector('p:last-of-type');
// lastParagraph.insertAdjacentElement('afterend', newLink);


// 2.

// const selectElement = document.createElement('select');
// selectElement.id = 'rating';

// const mainElement = document.querySelector('main');
// const featuresSection = mainElement.querySelector('.features');
// mainElement.insertBefore(selectElement, featuresSection);

// function createOption(value, text) {
//     const option = document.createElement('option');
//     option.value = value;
//     option.textContent = text;
//     return option;
// }

// selectElement.appendChild(createOption('4', '4 Stars'));
// selectElement.appendChild(createOption('3', '3 Stars'));
// selectElement.appendChild(createOption('2', '2 Stars'));
// selectElement.appendChild(createOption('1', '1 Star'));
